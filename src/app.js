'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    // We only allow transfer
    const action = req.query.action;
    const amount = parseInt(req.query.amount);
    if(!action || !amount){
        res.status(400).send('Something went wrong. Error: 11235');
    }

    try {
        if(validateInput(action, amount)) {
            res.send(payment(req.query.action, req.query.amount));
            return;
        }
    } catch(err) {
        res.status(400).send('Something went wrong. Error: 11235');
    }
});

function validateInput(action, amount) {
    if (typeof action !== 'string') {
        throw new Error('action cannot be null or non-string');
    }
    if (Number.isNaN(amount)) {
        throw new Error('amount cannot be null');
    }
    if (typeof amount !== 'number') {
        throw new Error('amount cannot be non-integer');
    }
    if (amount < 1 || amount > 10000) {
        throw new Error('amount must be an integer between 1 and 10000')
    }
    if (action !== 'transfer') {
        throw new Error('Only transfer allowed')
    }
    return true;
}


// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
